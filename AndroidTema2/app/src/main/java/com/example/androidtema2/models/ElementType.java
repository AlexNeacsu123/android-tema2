package com.example.androidtema2.models;

public enum ElementType {
    ARTIST,
    POST,
    ALBUM,
    PHOTOS
}
