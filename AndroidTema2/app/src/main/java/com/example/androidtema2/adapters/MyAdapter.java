package com.example.androidtema2.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.example.androidtema2.R;
import com.example.androidtema2.VolleyConfigSingleton;
import com.example.androidtema2.constants.Constants;
import com.example.androidtema2.interfaces.OnItemsClickedListener;
import com.example.androidtema2.models.Album;
import com.example.androidtema2.models.Element;
import com.example.androidtema2.models.Photos;
import com.example.androidtema2.models.Posts;
import com.example.androidtema2.models.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<Element> elementList;
    OnItemsClickedListener onItemsClickedListener;

    public MyAdapter(ArrayList<Element> elementList, OnItemsClickedListener onItemsClickedListener) {
        this.elementList = elementList;
        this.onItemsClickedListener = onItemsClickedListener;
    }

    public MyAdapter(ArrayList<Element> elementList) {
        this.elementList = elementList;
    }

    @Override
    public int getItemViewType(int position) {
        switch (elementList.get(position).getElement()) {
            case ARTIST:
                return 0;
            case POST:
                return 1;
            case ALBUM:
                return 2;
            case PHOTOS:
                return 3;
            default:
                return super.getItemViewType(position);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view;
        switch (viewType) {
            case 0:
                view = inflater.inflate(R.layout.item_artist, parent, false);
                ArtistViewHolder artistViewHolder = new ArtistViewHolder(view);
                return artistViewHolder;
            case 1:
                view = inflater.inflate(R.layout.item_posts, parent, false);
                PostsHolder postsHolder = new PostsHolder(view);
                return postsHolder;
            case 2:
                view = inflater.inflate(R.layout.item_album, parent, false);
                AlbumHolder albumsHolder = new AlbumHolder(view);
                return albumsHolder;
            case 3:
                view = inflater.inflate(R.layout.item_photos, parent, false);
                PhotosHolder photosHolder = new PhotosHolder(view);
                return photosHolder;
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArtistViewHolder) {
            User artist = (User) elementList.get(position);
            ((ArtistViewHolder) holder).bind(artist);
        } else if (holder instanceof PostsHolder) {
            Posts posts = (Posts) elementList.get(position);
            ((PostsHolder) holder).bind(posts);
        } else if (holder instanceof AlbumHolder) {
            Album album = (Album) elementList.get(position);
            ((AlbumHolder) holder).bind(album);
        } else if (holder instanceof PhotosHolder) {
            Photos photo = (Photos) elementList.get(position);
            ((PhotosHolder) holder).bind(photo);
        }
    }

    @Override
    public int getItemCount() {
        return this.elementList.size();
    }

    class ArtistViewHolder extends RecyclerView.ViewHolder {
        private final TextView name;
        private final TextView username;
        private final TextView email;
        private final ImageView imageButton;
        private final View view;

        ArtistViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.name);
            username = view.findViewById(R.id.username);
            email = view.findViewById(R.id.email);
            imageButton = view.findViewById(R.id.imageView);
            this.view = view;
        }


        void bind(User artist) {
            name.setText(artist.getName());
            username.setText(artist.getUsername());
            email.setText(artist.getEmail());
            imageButton.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (onItemsClickedListener != null) {
                                onItemsClickedListener.onImageClick(artist);
                                notifyItemChanged(getAdapterPosition());
                            }
                        }
                    });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onItemsClickedListener != null) {
                        onItemsClickedListener.onItemClick(artist);
                        notifyItemChanged(getAdapterPosition());
                    }
                }
            });
        }
    }

    class PostsHolder extends RecyclerView.ViewHolder {
        private final TextView title;
        private final TextView body;

        PostsHolder(View view) {
            super(view);
            title = view.findViewById(R.id.post_title);
            body = view.findViewById(R.id.body);
        }

        void bind(Posts posts) {
            title.setText(posts.getTitle());
            body.setText(posts.getBody());
        }
    }


    class AlbumHolder extends RecyclerView.ViewHolder {
        private final TextView body;
        private final View view;

        public AlbumHolder(@NonNull View view) {
            super(view);
            body = view.findViewById(R.id.album_body);
            this.view = view;
        }

        public void bind(Album album) {
            body.setText(album.getBody());
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemsClickedListener.onItemClick(album);
                    notifyItemChanged(getAdapterPosition());
                }
            });
        }
    }

    class PhotosHolder extends RecyclerView.ViewHolder {
        private final ImageView imageView;
        private final View view;

        public PhotosHolder(@NonNull View view) {
            super(view);
            imageView = view.findViewById(R.id.photos_body);
            this.view = view;
        }

        public void bind(Photos photos) {
            String imageViewUrl = photos.getUrl();
            Picasso picasso = Picasso.get();
            picasso.load(imageViewUrl).resize(Constants.IMAGE_WIDTH, Constants.IMAGE_HEIGHT).into(imageView);
        }
    }
}