package com.example.androidtema2.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.androidtema2.R;
import com.example.androidtema2.adapters.MyAdapter;
import com.example.androidtema2.constants.Constants;
import com.example.androidtema2.interfaces.ActivityFragmentCommunication;
import com.example.androidtema2.interfaces.OnItemsClickedListener;
import com.example.androidtema2.models.Album;
import com.example.androidtema2.models.Element;
import com.example.androidtema2.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Fragment2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragment2 extends Fragment implements OnItemsClickedListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private ActivityFragmentCommunication activityFragmentCommunication;
    private User currentUser;
    private View view;
    private ArrayList<Element> elements = new ArrayList<>();
    private MyAdapter myAdapter = null;
    private SwipeRefreshLayout swipeRefreshLayout;

    public Fragment2() {
        // Required empty public constructor
    }

    public Fragment2(Element currentUser) {
        this.currentUser = (User) currentUser;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Fragment2.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment2 newInstance(String param1, String param2) {
        Fragment2 fragment = new Fragment2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ActivityFragmentCommunication) {
            activityFragmentCommunication = (ActivityFragmentCommunication) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_2, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.album_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext(), RecyclerView.VERTICAL, false);
        swipeRefreshLayout = view.findViewById(R.id.swipe_rv_fragment2);
        elements.clear();
        getAlbums();
        myAdapter = new MyAdapter(this.elements, this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(this.myAdapter);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            elements.clear();
            myAdapter.notifyDataSetChanged();
            getAlbums();
            swipeRefreshLayout.setRefreshing(false);
        });
        return view;
    }


    void getAlbums() {
        RequestQueue queue = Volley.newRequestQueue(getContext());
        String url = Constants.BASE_URL + "/albums?" + Constants.USER_ID + "=" + currentUser.getId();
        StringRequest getAlbumsRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            handleAlbumResponse(response);
                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "ALBUMS ERROR", Toast.LENGTH_LONG).show();
                    }
                }
        );
        queue.add(getAlbumsRequest);
    }

    void handleAlbumResponse(String response) throws JSONException {
        JSONArray albumJSONArray = new JSONArray(response);
        for (int index = 0; index < albumJSONArray.length(); ++index) {
            JSONObject userPostJSON = (JSONObject) albumJSONArray.get(index);
            int id = userPostJSON.getInt("id");
            String title = userPostJSON.getString("title");
            Album album = new Album(id, title);
            if (!elements.contains(album))
                this.elements.add(album);

        }
        myAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(Element element) {
        activityFragmentCommunication.openFragment3(element);
    }

    @Override
    public void onImageClick(User user) {

    }
}