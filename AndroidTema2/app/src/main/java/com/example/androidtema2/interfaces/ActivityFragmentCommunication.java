package com.example.androidtema2.interfaces;

import com.example.androidtema2.fragments.Fragment1;
import com.example.androidtema2.models.Album;
import com.example.androidtema2.models.Element;
import com.example.androidtema2.models.User;

public interface ActivityFragmentCommunication {
    void openFragment1();
    void openFragment2(Element user);
    void openFragment3(Element album);
}
