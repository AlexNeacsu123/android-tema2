package com.example.androidtema2.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.androidtema2.R;
import com.example.androidtema2.VolleyConfigSingleton;
import com.example.androidtema2.adapters.MyAdapter;
import com.example.androidtema2.constants.Constants;
import com.example.androidtema2.interfaces.ActivityFragmentCommunication;
import com.example.androidtema2.interfaces.OnItemsClickedListener;
import com.example.androidtema2.models.Element;
import com.example.androidtema2.models.Posts;
import com.example.androidtema2.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Console;
import java.util.ArrayList;
import java.util.Stack;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link Fragment1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragment1 extends Fragment implements OnItemsClickedListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ActivityFragmentCommunication activityFragmentCommunication;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View view;
    private ArrayList<Element> elements = new ArrayList<>();
    private ArrayList<Posts> posts = new ArrayList<>();
    private MyAdapter myAdapter = null;

    public Fragment1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Fragment1.
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment1 newInstance(String param1, String param2) {
        Fragment1 fragment = new Fragment1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_1, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.artist_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext(), RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        swipeRefreshLayout = view.findViewById(R.id.swipe_rv_fragment1);
        getUserDetails();
        myAdapter = new MyAdapter(elements, this);
        recyclerView.setAdapter(myAdapter);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            elements.clear();
            myAdapter.notifyDataSetChanged();
            getUserDetails();
            swipeRefreshLayout.setRefreshing(false);
        });
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ActivityFragmentCommunication) {
            activityFragmentCommunication = (ActivityFragmentCommunication) context;
        }
    }


    void getUserDetails() {
        VolleyConfigSingleton volleyConfigSingleton = VolleyConfigSingleton.getInstance(view.getContext());
        RequestQueue queue = volleyConfigSingleton.getRequestQueue();
        String url = Constants.BASE_URL + "/users";
        StringRequest getAlbumsRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            handleUsersResponse(response);
                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "USERS ERROR", Toast.LENGTH_LONG).show();
                    }
                }
        );
        queue.add(getAlbumsRequest);
    }


    void getPosts(User user) {
        VolleyConfigSingleton volleyConfigSingleton = VolleyConfigSingleton.getInstance(this.getContext());
        RequestQueue queue = volleyConfigSingleton.getRequestQueue();
        String url = Constants.BASE_URL + "/posts?" + Constants.USER_ID + "=" + user.getId();
        StringRequest getAlbumsRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            handlePostsResponse(response);
                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(), "POSTS ERROR", Toast.LENGTH_LONG).show();
                    }
                }

        );
        queue.add(getAlbumsRequest);
    }

    void handlePostsResponse(String response) throws JSONException {
        JSONArray postsJSONArray = new JSONArray(response);
        for (int index = 0; index < postsJSONArray.length(); ++index) {
            JSONObject userPostJSON = (JSONObject) postsJSONArray.get(index);
            int id = userPostJSON.getInt("id");
            int userId = userPostJSON.getInt("userId");
            String title = userPostJSON.getString("title");
            String body = userPostJSON.getString("body");
            Posts newPost = new Posts(userId, id, title, body);
            if (!posts.contains(newPost))
                posts.add(newPost);
        }
        myAdapter.notifyDataSetChanged();
    }


    void handleUsersResponse(String response) throws JSONException {
        JSONArray postsJSONArray = new JSONArray(response);
        for (int index = 0; index < postsJSONArray.length(); ++index) {
            JSONObject userPostJSON = (JSONObject) postsJSONArray.get(index);
            int id = userPostJSON.getInt("id");
            String name = userPostJSON.getString("name");
            String username = userPostJSON.getString("username");
            String email = userPostJSON.getString("email");
            elements.add(new User(id, name, username, email));
        }
        myAdapter.notifyDataSetChanged();
    }

    private void deletePosts(User user) {
        Stack<Element> postsToDelete = new Stack<>();

        for (Element element : elements) {
            if (element instanceof Posts && ((Posts) element).getUserId() == user.getId()) {
                postsToDelete.add(element);
            }
        }
        while (!postsToDelete.empty()) {
            elements.remove(postsToDelete.pop());
        }
        myAdapter.notifyDataSetChanged();
    }

    private void reorderElements() {
        ArrayList<Element> reorderedElements = new ArrayList<>();
        for (Element element : elements) {
            if (element instanceof User) {
                reorderedElements.add(element);
                if (((User) element).isExpandPosts()) {
                    for (Posts post : posts) {
                        if (((User) element).getId() == post.getUserId()) {
                            reorderedElements.add(post);
                        }
                    }
                }
            }
        }
        elements.clear();
        elements.addAll(reorderedElements);
    }

    @Override
    public void onItemClick(Element user) {
        activityFragmentCommunication.openFragment2(user);
    }

    @Override
    public void onImageClick(User user) {
        getPosts(user);
        if (!user.isExpandPosts()) {
            user.setExpandPosts(true);
            reorderElements();
            myAdapter.notifyDataSetChanged();
        } else {
            user.setExpandPosts(false);
            deletePosts(user);
        }
    }
}