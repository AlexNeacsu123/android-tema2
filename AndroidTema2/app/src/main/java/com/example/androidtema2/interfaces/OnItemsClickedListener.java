package com.example.androidtema2.interfaces;

import com.example.androidtema2.models.Element;
import com.example.androidtema2.models.User;

public interface OnItemsClickedListener {
    void onItemClick(Element element);
    void onImageClick(User user);
}
